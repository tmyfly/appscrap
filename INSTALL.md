1. Building the project:
```
mvn clean package spring-boot:repackage
```

2. Building the docker:

```
docker-compose build
```

3. Running the docker:
```
docker-compose up
```
4. Enter the address in the browser:
```
http://localhost:8080/
```
5. Choose a category. After selecting a category and clicking the "Apply" button, please wait for the data to load. Upon completion, you will be redirected to the page with the results.

6. Stop the application: Ctrl+C (Windows)

7. Stopping and removing containers:
```
docker-compose down
```
<br>

___

1. Сборка проекта
```
mvn clean package spring-boot:repackage
```
2. Сборка docker
```
docker-compose build
```
3. Запуск docker
```
docker-compose up
```
4. В броузере ввести адрес
```
http://localhost:8080/
```
5. Вибрать категорию. После выбора категории и нажатия на кнопку Apply необходимо подождать пока загрузятся данные, по окончанию работы произойдет ридерект на страницу с результатом

6. Останавить приложение Ctrl+C (windows)

7. Останавить и удалить контейнеры
```
docker-compose down
```
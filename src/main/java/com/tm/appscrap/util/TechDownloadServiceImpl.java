package com.tm.appscrap.util;

import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collections;

@Service
public class TechDownloadServiceImpl implements JsonDownloadService{

    @Override
    public String download(String urlJson) throws InterruptedException {
        System.out.println(urlJson);
        OkHttpClient client = new OkHttpClient.Builder()
                .protocols(Collections.singletonList(Protocol.HTTP_1_1))
                .build();
        Request request = new Request.Builder().url(urlJson).build();

        Thread.sleep(1000);

        try (Response response = client.newCall(request).execute()) {
            assert response.body() != null;
            return response.body().string();
        } catch (IOException e) {
            throw new InterruptedException();
        }
    }
}

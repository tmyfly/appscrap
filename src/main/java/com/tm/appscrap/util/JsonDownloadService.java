package com.tm.appscrap.util;

public interface JsonDownloadService {
    String download(String url) throws InterruptedException;

}

package com.tm.appscrap.controller;

import com.tm.appscrap.app.ScrapApp;
import com.tm.appscrap.model.DataPoint;
import com.tm.appscrap.service.DataPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/")
public class DataController {
    @Autowired
    private ScrapApp scrapApp;

    @Autowired
    private DataPointService dataPointService;

    @GetMapping("/")
    public String showHomePage() {
        return "home";
    }

    @PostMapping("/apply")
    public String processApplication(@RequestParam("jobFunction") String jobFunction) {
        scrapApp.run(jobFunction);

        return "redirect:/data-points/" + jobFunction;
    }

    @GetMapping("/data-points")
    public String showDataPoints(@RequestParam(name = "sortColumn", required = false, defaultValue = "category") String sortColumn,
                                 @RequestParam(name = "sortOrder", required = false, defaultValue = "asc") String sortOrder,
                                 Model model) {
        Sort.Direction direction = sortOrder.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Sort sort = Sort.by(direction, sortColumn);

        List<DataPoint> dataPoints = dataPointService.getAllDataPointsSorted(sort);
        model.addAttribute("dataPoints", dataPoints);
        return "data";
    }

    @GetMapping("/data-points/{category}")
    public String getDataPointsByCategory(@PathVariable(name = "category") String category,
                                          @RequestParam(name = "sortColumn", required = false, defaultValue = "category") String sortColumn,
                                          @RequestParam(name = "sortOrder", required = false, defaultValue = "asc") String sortOrder,
                                          Model model) {
        Sort.Direction direction = sortOrder.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Sort sort = Sort.by(direction, sortColumn);

        List<DataPoint> dataPoints = dataPointService.getDataPointsByCategoryAndSort(category, sort);
        model.addAttribute("dataPoints", dataPoints);
        return "data";
    }
}

package com.tm.appscrap.app;

import com.google.gson.Gson;
import com.tm.appscrap.model.DataPoint;
import com.tm.appscrap.model.JobItem;
import com.tm.appscrap.model.Location;
import com.tm.appscrap.model.TagsNames;
import com.tm.appscrap.model.techmodel.ResultJson;
import com.tm.appscrap.model.techmodelpage.Root;
import com.tm.appscrap.service.DataPointService;
import com.tm.appscrap.util.JsonDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
public class ScrapApp {
    @Autowired
    private JsonDownloadService jsonDownloadService;

    @Autowired
    private DataPointService dataPointService;

    List<JobItem> itemListGlob = null;

    public boolean run(String jobFunction) {
        boolean result = false;

        try {
            if(itemListGlob == null){
                itemListGlob = getListItems(jobFunction);
            }
            createData(itemListGlob, jobFunction);
            result = true;
            itemListGlob =null;
        } catch (InterruptedException e) {
            run(jobFunction);
        }
        return result;
    }

    private void createData(List<JobItem> itemList, String jobFunction) throws InterruptedException {
        int count = itemList.size();
        for (var item : itemList) {
            System.out.println("Size " + itemList.size() + " left " + count);
            count--;
            if (dataPointService.isPresent(item.getCreatedAt(), item.getTitle(), item.getUrl())) {
                continue;
            }
            DataPoint dataPoint = new DataPoint();
            dataPoint.setPositionName(item.getTitle());
            dataPoint.setUrlToOrganization(item.getUrl());
            dataPoint.setLogo(item.getLogoUrl());
            dataPoint.setOrganizationTitle(item.getOrganizationName());
            dataPoint.setLaborFunction(item.getLaborFunction());
            dataPoint.setLocation(item.getLocations().stream().map(Location::new).toList());
            dataPoint.setPostedDate(item.getCreatedAt());
            dataPoint.setTagsNames(item.getIndustryTags().stream().map(TagsNames::new).toList());
            dataPoint.setCategory(jobFunction);

            if (!item.getHasDescription()) {
                dataPoint.setJobPageUrl("");
                dataPoint.setDescription("");

                dataPointService.saveData(dataPoint);
            } else {
                String urlJob = "https://jobs.techstars.com/companies/";
                dataPoint.setJobPageUrl(urlJob + item.getOrganizationSlug() + "/jobs/" + item.getSlug() + "#content");
                dataPoint.setDescription(getDescription(dataPoint.getJobPageUrl()));

                dataPointService.saveData(dataPoint);

                Thread.sleep(2000);
            }
        }
    }

    private String getDescription(String itemUrl) throws InterruptedException {
        var gson = new Gson();
        String job = jsonDownloadService.download(itemUrl);

        var startIndex = job.indexOf("type=\"application/json\">");
        var endIndex = job.indexOf("</script></body>");
        var result = job.substring(startIndex + 24, endIndex);

        Root root = gson.fromJson(result, Root.class);
        var jobs = root.props.pageProps.initialState.jobs;
        return jobs.currentJob.description;
    }

    public List<JobItem> getListItems(String jobFunction) throws InterruptedException {
        int nbPages = 1;
        List<JobItem> itemList = new ArrayList<>();

        for (int i = 0; i < nbPages; i++) {
            String url = "https://jobs.techstars.com/api/search/jobs?networkId=89&hitsPerPage=20&page=" + i + "&filters=%28job_functions%3A%22" + jobFunction + "%22%29&query=";
            String json = jsonDownloadService.download(url);
            var gson = new Gson();
            ResultJson rootJson = gson.fromJson(json, ResultJson.class);
            var list = rootJson.getResults().get(0).getHits();
            nbPages = rootJson.getResults().get(0).getNbPages();
            list.forEach(el -> {
                JobItem jobItem = new JobItem();
                jobItem.setCreatedAt(el.getCreatedAt());
                jobItem.setLocations(new HashSet<>(el.getLocations()));
                jobItem.setOrganizationName(el.getOrganization().getName());
                jobItem.setLogoUrl(el.getOrganization().getLogoUrl());
                jobItem.setOrganizationSlug(el.getOrganization().getSlug());
                jobItem.setIndustryTags(new HashSet<>(el.getOrganization().getIndustryTags()));
                jobItem.setSlug(el.getSlug());
                jobItem.setTitle(el.getTitle());
                jobItem.setUrl(el.getUrl());
                jobItem.setHasDescription(el.getHasDescription());
                jobItem.setLaborFunction(el.getHighlightResult().getJobFunctions().get(0).getValue());
                itemList.add(jobItem);
            });
        }
        return itemList;
    }
}

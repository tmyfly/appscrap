package com.tm.appscrap.model;

import lombok.Data;

import java.util.Set;

@Data
public class JobItem {
    private long createdAt;
    private Set<String> locations;
    private String organizationName;
    private String logoUrl;
    private String organizationSlug;
    private Set<String> industryTags;
    private String slug;
    private String title;
    private String url;
    private Boolean hasDescription;
    private String laborFunction;
}

package com.tm.appscrap.model.techmodel;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class HighlightResult {

    @SerializedName("job_functions")
    List<JobFunctions> jobFunctions;

    @SerializedName("organization")
    Organization1 organization;

    @SerializedName("title")
    Title title;

    //   @SerializedName("description")
    transient List<Description> description;


    public void setJobFunctions(List<JobFunctions> jobFunctions) {
        this.jobFunctions = jobFunctions;
    }

    public List<JobFunctions> getJobFunctions() {
        return jobFunctions;
    }

    public void setOrganization(Organization1 organization) {
        this.organization = organization;
    }

    public Organization1 getOrganization() {
        return organization;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public Title getTitle() {
        return title;
    }

    public void setDescription(List<Description> description) {
        this.description = description;
    }

    public List<Description> getDescription() {
        return description;
    }

}
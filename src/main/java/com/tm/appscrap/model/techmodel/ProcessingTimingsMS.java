package com.tm.appscrap.model.techmodel;

import com.google.gson.annotations.SerializedName;

   
public class ProcessingTimingsMS {

   @SerializedName("afterFetch")
   AfterFetch afterFetch;

   @SerializedName("getIdx")
   GetIdx getIdx;

   @SerializedName("request")
   Request request;

   @SerializedName("total")
   int total;


    public void setAfterFetch(AfterFetch afterFetch) {
        this.afterFetch = afterFetch;
    }
    public AfterFetch getAfterFetch() {
        return afterFetch;
    }
    
    public void setGetIdx(GetIdx getIdx) {
        this.getIdx = getIdx;
    }
    public GetIdx getGetIdx() {
        return getIdx;
    }
    
    public void setRequest(Request request) {
        this.request = request;
    }
    public Request getRequest() {
        return request;
    }
    
    public void setTotal(int total) {
        this.total = total;
    }
    public int getTotal() {
        return total;
    }
    
}
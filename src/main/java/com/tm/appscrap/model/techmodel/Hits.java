package com.tm.appscrap.model.techmodel;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;


//@JsonIgnoreProperties(ignoreUnknown = true)
public class Hits {

   @SerializedName("created_at")
   int createdAt;

   @SerializedName("locations")
   List<String> locations;

   @SerializedName("organization")
   Organization organization;

   @SerializedName("source")
   String source;

   @SerializedName("slug")
   String slug;

   @SerializedName("title")
   String title;

   @SerializedName("url")
   String url;

   @SerializedName("featured")
   boolean featured;

   @SerializedName("has_description")
   boolean hasDescription;

   @SerializedName("objectID")
   String objectID;

   @SerializedName("_highlightResult")
   HighlightResult HighlightResult;


    public void setCreatedAt(int createdAt) {
        this.createdAt = createdAt;
    }
    public int getCreatedAt() {
        return createdAt;
    }
    
    public void setLocations(List<String> locations) {
        this.locations = locations;
    }
    public List<String> getLocations() {
        return locations;
    }
    
    public void setOrganization(Organization organization) {
        this.organization = organization;
    }
    public Organization getOrganization() {
        return organization;
    }
    
    public void setSource(String source) {
        this.source = source;
    }
    public String getSource() {
        return source;
    }
    
    public void setSlug(String slug) {
        this.slug = slug;
    }
    public String getSlug() {
        return slug;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    public String getUrl() {
        return url;
    }
    
    public void setFeatured(boolean featured) {
        this.featured = featured;
    }
    public boolean getFeatured() {
        return featured;
    }
    
    public void setHasDescription(boolean hasDescription) {
        this.hasDescription = hasDescription;
    }
    public boolean getHasDescription() {
        return hasDescription;
    }
    
    public void setObjectID(String objectID) {
        this.objectID = objectID;
    }
    public String getObjectID() {
        return objectID;
    }
    
    public void setHighlightResult(HighlightResult HighlightResult) {
        this.HighlightResult = HighlightResult;
    }
    public HighlightResult getHighlightResult() {
        return HighlightResult;
    }

    @Override
    public String toString() {
        return "Hits{" +
                "createdAt=" + createdAt +
                ", locations=" + locations +
                ", organization=" + organization +
                ", source='" + source + '\'' +
                ", slug='" + slug + '\'' +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", featured=" + featured +
                ", hasDescription=" + hasDescription +
                ", objectID='" + objectID + '\'' +
                ", HighlightResult=" + HighlightResult +
                '}';
    }
}
package com.tm.appscrap.model.techmodel;

import com.google.gson.annotations.SerializedName;

   
public class AfterFetch {

   @SerializedName("format")
   Format format;

   @SerializedName("total")
   int total;


    public void setFormat(Format format) {
        this.format = format;
    }
    public Format getFormat() {
        return format;
    }
    
    public void setTotal(int total) {
        this.total = total;
    }
    public int getTotal() {
        return total;
    }
    
}
package com.tm.appscrap.model.techmodel;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.SerializedName;


//@JsonIgnoreProperties(ignoreUnknown = true)
public class Id {

   @SerializedName("value")
   int value;

   @SerializedName("matchLevel")
   String matchLevel;

   @SerializedName("matchedWords")
   List<String> matchedWords;


    public void setValue(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
    
    public void setMatchLevel(String matchLevel) {
        this.matchLevel = matchLevel;
    }
    public String getMatchLevel() {
        return matchLevel;
    }
    
    public void setMatchedWords(List<String> matchedWords) {
        this.matchedWords = matchedWords;
    }
    public List<String> getMatchedWords() {
        return matchedWords;
    }
    
}
package com.tm.appscrap.model.techmodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Organization1 {

    @SerializedName("id")
    Id id;

    @SerializedName("name")
    Name name;

    @SerializedName("industry_tags")
    List<IndustryTags> industryTags;

    @SerializedName("stage")
    Stage stage;

    @SerializedName("head_count")
    HeadCount headCount;


    public void setId(Id id) {
        this.id = id;
    }
    public Id getId() {
        return id;
    }

    public void setName(Name name) {
        this.name = name;
    }
    public Name getName() {
        return name;
    }

    public void setIndustryTags(List<IndustryTags> industryTags) {
        this.industryTags = industryTags;
    }
    public List<IndustryTags> getIndustryTags() {
        return industryTags;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
    public Stage getStage() {
        return stage;
    }

    public void setHeadCount(HeadCount headCount) {
        this.headCount = headCount;
    }
    public HeadCount getHeadCount() {
        return headCount;
    }

}
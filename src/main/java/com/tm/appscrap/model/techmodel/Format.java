package com.tm.appscrap.model.techmodel;

import com.google.gson.annotations.SerializedName;

   
public class Format {

   @SerializedName("highlighting")
   int highlighting;

   @SerializedName("total")
   int total;


    public void setHighlighting(int highlighting) {
        this.highlighting = highlighting;
    }
    public int getHighlighting() {
        return highlighting;
    }
    
    public void setTotal(int total) {
        this.total = total;
    }
    public int getTotal() {
        return total;
    }
    
}
package com.tm.appscrap.model.techmodel;

import com.google.gson.annotations.SerializedName;

   
public class GetIdx {

   @SerializedName("load")
   Load load;

   @SerializedName("total")
   int total;


    public void setLoad(Load load) {
        this.load = load;
    }
    public Load getLoad() {
        return load;
    }
    
    public void setTotal(int total) {
        this.total = total;
    }
    public int getTotal() {
        return total;
    }
    
}
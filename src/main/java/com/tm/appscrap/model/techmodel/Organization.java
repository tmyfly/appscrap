package com.tm.appscrap.model.techmodel;
import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Organization {

    @SerializedName("id")
    int id;

    @SerializedName("name")
    String name;

    @SerializedName("logo_url")
    String logoUrl;

    @SerializedName("slug")
    String slug;

    @SerializedName("topics")
    List<String> topics;

    @SerializedName("industry_tags")
    List<String> industryTags;

    @SerializedName("stage")
    String stage;

    @SerializedName("head_count")
    int headCount;


    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }
    public String getLogoUrl() {
        return logoUrl;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
    public String getSlug() {
        return slug;
    }

    public void setTopics(List<String> topics) {
        this.topics = topics;
    }
    public List<String> getTopics() {
        return topics;
    }

    public void setIndustryTags(List<String> industryTags) {
        this.industryTags = industryTags;
    }
    public List<String> getIndustryTags() {
        return industryTags;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }
    public String getStage() {
        return stage;
    }

    public void setHeadCount(int headCount) {
        this.headCount = headCount;
    }
    public int getHeadCount() {
        return headCount;
    }

    @Override
    public String toString() {
        return "Organization{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", logoUrl='" + logoUrl + '\'' +
                ", slug='" + slug + '\'' +
                ", topics=" + topics +
                ", industryTags=" + industryTags +
                ", stage='" + stage + '\'' +
                ", headCount=" + headCount +
                '}';
    }
}
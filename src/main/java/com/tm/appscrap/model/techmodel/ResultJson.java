package com.tm.appscrap.model.techmodel;
import java.util.List;

import com.google.gson.annotations.SerializedName;

   
public class ResultJson {

   @SerializedName("results")
   List<Results> results;


    public void setResults(List<Results> results) {
        this.results = results;
    }
    public List<Results> getResults() {
        return results;
    }
    
}
package com.tm.appscrap.model.techmodel;

import com.google.gson.annotations.SerializedName;

   
public class Load {

   @SerializedName("total")
   int total;


    public void setTotal(int total) {
        this.total = total;
    }
    public int getTotal() {
        return total;
    }
    
}
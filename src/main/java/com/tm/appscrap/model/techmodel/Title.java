package com.tm.appscrap.model.techmodel;
import java.util.List;

import com.google.gson.annotations.SerializedName;

   
public class Title {

   @SerializedName("value")
   String value;

   @SerializedName("matchLevel")
   String matchLevel;

   @SerializedName("matchedWords")
   List<String> matchedWords;


    public void setValue(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
    
    public void setMatchLevel(String matchLevel) {
        this.matchLevel = matchLevel;
    }
    public String getMatchLevel() {
        return matchLevel;
    }
    
    public void setMatchedWords(List<String> matchedWords) {
        this.matchedWords = matchedWords;
    }
    public List<String> getMatchedWords() {
        return matchedWords;
    }
    
}
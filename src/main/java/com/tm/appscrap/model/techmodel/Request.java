package com.tm.appscrap.model.techmodel;

import com.google.gson.annotations.SerializedName;

   
public class Request {

   @SerializedName("roundTrip")
   int roundTrip;


    public void setRoundTrip(int roundTrip) {
        this.roundTrip = roundTrip;
    }
    public int getRoundTrip() {
        return roundTrip;
    }
    
}
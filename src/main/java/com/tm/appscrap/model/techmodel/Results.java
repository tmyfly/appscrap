package com.tm.appscrap.model.techmodel;
import java.util.List;

import com.google.gson.annotations.SerializedName;

   
public class Results {

   @SerializedName("hits")
   List<Hits> hits;

   @SerializedName("nbHits")
   int nbHits;

   @SerializedName("page")
   int page;

   @SerializedName("nbPages")
   int nbPages;

   @SerializedName("hitsPerPage")
   int hitsPerPage;

   @SerializedName("exhaustiveNbHits")
   boolean exhaustiveNbHits;

   @SerializedName("exhaustiveTypo")
   boolean exhaustiveTypo;

   @SerializedName("exhaustive")
   Exhaustive exhaustive;

   @SerializedName("query")
   String query;

   @SerializedName("params")
   String params;

   @SerializedName("index")
   String index;

   @SerializedName("renderingContent")
   RenderingContent renderingContent;

   @SerializedName("processingTimeMS")
   int processingTimeMS;

   @SerializedName("processingTimingsMS")
   ProcessingTimingsMS processingTimingsMS;

   @SerializedName("serverTimeMS")
   int serverTimeMS;


    public void setHits(List<Hits> hits) {
        this.hits = hits;
    }
    public List<Hits> getHits() {
        return hits;
    }
    
    public void setNbHits(int nbHits) {
        this.nbHits = nbHits;
    }
    public int getNbHits() {
        return nbHits;
    }
    
    public void setPage(int page) {
        this.page = page;
    }
    public int getPage() {
        return page;
    }
    
    public void setNbPages(int nbPages) {
        this.nbPages = nbPages;
    }
    public int getNbPages() {
        return nbPages;
    }
    
    public void setHitsPerPage(int hitsPerPage) {
        this.hitsPerPage = hitsPerPage;
    }
    public int getHitsPerPage() {
        return hitsPerPage;
    }
    
    public void setExhaustiveNbHits(boolean exhaustiveNbHits) {
        this.exhaustiveNbHits = exhaustiveNbHits;
    }
    public boolean getExhaustiveNbHits() {
        return exhaustiveNbHits;
    }
    
    public void setExhaustiveTypo(boolean exhaustiveTypo) {
        this.exhaustiveTypo = exhaustiveTypo;
    }
    public boolean getExhaustiveTypo() {
        return exhaustiveTypo;
    }
    
    public void setExhaustive(Exhaustive exhaustive) {
        this.exhaustive = exhaustive;
    }
    public Exhaustive getExhaustive() {
        return exhaustive;
    }
    
    public void setQuery(String query) {
        this.query = query;
    }
    public String getQuery() {
        return query;
    }
    
    public void setParams(String params) {
        this.params = params;
    }
    public String getParams() {
        return params;
    }
    
    public void setIndex(String index) {
        this.index = index;
    }
    public String getIndex() {
        return index;
    }
    
    public void setRenderingContent(RenderingContent renderingContent) {
        this.renderingContent = renderingContent;
    }
    public RenderingContent getRenderingContent() {
        return renderingContent;
    }
    
    public void setProcessingTimeMS(int processingTimeMS) {
        this.processingTimeMS = processingTimeMS;
    }
    public int getProcessingTimeMS() {
        return processingTimeMS;
    }
    
    public void setProcessingTimingsMS(ProcessingTimingsMS processingTimingsMS) {
        this.processingTimingsMS = processingTimingsMS;
    }
    public ProcessingTimingsMS getProcessingTimingsMS() {
        return processingTimingsMS;
    }
    
    public void setServerTimeMS(int serverTimeMS) {
        this.serverTimeMS = serverTimeMS;
    }
    public int getServerTimeMS() {
        return serverTimeMS;
    }
    
}
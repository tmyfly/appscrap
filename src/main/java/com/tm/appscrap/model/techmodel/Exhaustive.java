package com.tm.appscrap.model.techmodel;

import com.google.gson.annotations.SerializedName;

   
public class Exhaustive {

   @SerializedName("nbHits")
   boolean nbHits;

   @SerializedName("typo")
   boolean typo;


    public void setNbHits(boolean nbHits) {
        this.nbHits = nbHits;
    }
    public boolean getNbHits() {
        return nbHits;
    }
    
    public void setTypo(boolean typo) {
        this.typo = typo;
    }
    public boolean getTypo() {
        return typo;
    }
    
}
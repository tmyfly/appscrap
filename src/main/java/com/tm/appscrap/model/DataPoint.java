package com.tm.appscrap.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class DataPoint {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(length = 1001)
    private String jobPageUrl; // on jobs.techstars.com
    private String positionName;
    @Column(length = 1002)
    private String urlToOrganization;
    @Column(length = 1003)
    private String logo; // (save only link to image)
    private String organizationTitle;
    private String laborFunction;
    @ManyToMany(cascade = {CascadeType.ALL})
    private List<Location> location = new ArrayList<>(); //(pars and save by parts if possible, it can be multiple)
    private Long postedDate; // (save in Unix Timestamp)
    @Column(length = 50000)
    private String description; // (save with html formatting)
    @ManyToMany(cascade = {CascadeType.ALL})
    private List<TagsNames> tagsNames = new ArrayList<>(); // (can be multiple)

    private String category;

}

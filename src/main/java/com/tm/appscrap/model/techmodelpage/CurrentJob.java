package com.tm.appscrap.model.techmodelpage; 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
public class CurrentJob{
    public String applicationMethod;
    public Object applicationPath;
    public boolean compensationPublic;
    public String description;
    public boolean discarded;
    public ArrayList<Object> employmentTypes;
    public Object expiresAt;
    public int id;
    public boolean liked;
    public ArrayList<Location> locations;
    public Date postedAt;
    public String slug;
    public String source;
    public String status;
    public String title;
    public String url;
    public Object deactivatedAt;
    public Object closedAt;
    public boolean passesFilter;
    public String visibility;
    public ArrayList<JobFunction> jobFunctions;
    public Organization organization;
    public Object author;
    public Object bounty;

    @Override
    public String toString() {
        return "CurrentJob{" +
                "applicationMethod='" + applicationMethod + '\'' +
                ", applicationPath=" + applicationPath +
                ", compensationPublic=" + compensationPublic +
                ", description='" + description + '\'' +
                ", discarded=" + discarded +
                ", employmentTypes=" + employmentTypes +
                ", expiresAt=" + expiresAt +
                ", id=" + id +
                ", liked=" + liked +
                ", locations=" + locations +
                ", postedAt=" + postedAt +
                ", slug='" + slug + '\'' +
                ", source='" + source + '\'' +
                ", status='" + status + '\'' +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", deactivatedAt=" + deactivatedAt +
                ", closedAt=" + closedAt +
                ", passesFilter=" + passesFilter +
                ", visibility='" + visibility + '\'' +
                ", jobFunctions=" + jobFunctions +
                ", organization=" + organization +
                ", author=" + author +
                ", bounty=" + bounty +
                '}';
    }
}

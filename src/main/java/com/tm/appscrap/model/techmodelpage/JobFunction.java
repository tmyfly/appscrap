package com.tm.appscrap.model.techmodelpage; 
public class JobFunction{
    public int id;
    public String name;

    @Override
    public String toString() {
        return "JobFunction{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

package com.tm.appscrap.model.techmodelpage; 
public class JobBoardFilters{
    public boolean stageFilter;
    public boolean locationFilter;
    public boolean companySizeFilter;
    public boolean jobFunctionFilter;
    public boolean industryTagsFilter;
}

package com.tm.appscrap.model.techmodelpage; 
import java.util.ArrayList;
import java.util.List;
public class LocationSuggestions{
    public String term;
    public ArrayList<Object> suggestions;
    public boolean initialized;
    public boolean loading;
}

package com.tm.appscrap.model.techmodelpage; 
import java.util.ArrayList;
import java.util.List;
public class FormData{
    public String avatarUrl;
    public String linkedinId;
    public String firstName;
    public String lastName;
    public String email;
    public boolean mailTnTlJobAlerts;
    public String password;
    public boolean termsOfService;
    public String websiteUrl;
    public String linkedinUrl;
    public String dribbbleUrl;
    public String githubUrl;
    public Object currentLocation;
    public ArrayList<Object> resume;
    public String bio;
    public boolean mentorship;
    public String jobSearchStatus;
    public ArrayList<Object> employmentTypes;
    public String seniority;
    public ArrayList<Object> skills;
    public ArrayList<Object> locations;
    public boolean willWorkAnywhere;
    public boolean remoteWork;
    public boolean usWorkAuthorization;
    public boolean requireSponsorshipVisa;
    public Visibility visibility;
    public ArrayList<Object> hideFromCompanies;
    public ArrayList<Object> hideFromDomains;
    public UserJobAlertPreference userJobAlertPreference;
}

package com.tm.appscrap.model.techmodelpage; 
public class Legal{
    public String link;
    public String name;
    public String email;
    public String address;
    public boolean ccpaCompliant;
    public String ccpaPolicyUrl;
    public String dataPrivacyRegulatoryBody;
    public String dataPrivacyRegulatoryNumber;
}

package com.tm.appscrap.model.techmodelpage; 
public class Location{
    public int id;
    public String name;
    public String placeId;
    public String description;

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", placeId='" + placeId + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}

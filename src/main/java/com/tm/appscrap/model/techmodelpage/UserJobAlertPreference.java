package com.tm.appscrap.model.techmodelpage; 
import java.util.ArrayList;
import java.util.List;
public class UserJobAlertPreference{
    public ArrayList<Object> keywords;
    public ArrayList<Object> locations;
    public boolean remoteWork;
    public boolean willWorkAnywhere;
    public Frequency frequency;
}

package com.tm.appscrap.model.techmodelpage; 
import java.util.ArrayList;
import java.util.List;
public class AdvancedDesign{
    public Theme theme;
    public String footer;
    public String header;
    public ArrayList<String> scripts;
    public ArrayList<Object> linkedScripts;
    public boolean initialized;
    public boolean isScriptLoaded;
}

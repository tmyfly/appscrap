package com.tm.appscrap.model.techmodelpage; 
public class PageProps{
    public AdvancedDesign advancedDesign;
    public Network network;
    public String _sentryTraceData;
    public String _sentryBaggage;
    public String protocol;
    public String host;
    public InitialState initialState;
}

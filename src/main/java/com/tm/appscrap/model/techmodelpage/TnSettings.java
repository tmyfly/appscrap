package com.tm.appscrap.model.techmodelpage; 
public class TnSettings{
    public String tnCtaTitleText;
    public String tnCtaButtonText;
    public String tnCtaDescriptionText;
    public Object tnV2TabIntroductionTitle;
    public Object tnV2TabIntroductionText;
    public Object tnV2SuccessMessageTitle;
    public Object tnV2SuccessMessageText;
    public String tnTabIntroductionText;
    public String tnSignUpWelcomeText;
    public boolean tnHidden;
    public String tnSignUpWelcomeTitle;
}

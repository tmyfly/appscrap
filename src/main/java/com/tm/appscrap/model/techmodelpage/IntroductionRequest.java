package com.tm.appscrap.model.techmodelpage; 
public class IntroductionRequest{
    public boolean visible;
    public boolean error;
    public boolean introductionSent;
    public int limit;
    public int remaining;
}

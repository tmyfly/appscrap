package com.tm.appscrap.model.techmodelpage; 
import java.util.ArrayList;
import java.util.List;
public class Root{
    public Props props;
    public String page;
    public Query query;
    public String buildId;
    public RuntimeConfig runtimeConfig;
    public boolean isFallback;
    public boolean gssp;
    public boolean appGip;
    public ArrayList<Object> scriptLoader;
}

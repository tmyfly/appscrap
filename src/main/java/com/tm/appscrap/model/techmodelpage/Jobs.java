package com.tm.appscrap.model.techmodelpage; 
import java.util.ArrayList;
import java.util.List;
public class Jobs{
    public ArrayList<Object> found;
    public CurrentJob currentJob;
    public int total;
    public boolean initialized;
    public boolean loading;
    public boolean isLazyLoading;

    @Override
    public String toString() {
        return "Jobs{" +
                "found=" + found +
                ", currentJob=" + currentJob +
                ", total=" + total +
                ", initialized=" + initialized +
                ", loading=" + loading +
                ", isLazyLoading=" + isLazyLoading +
                '}';
    }
}

package com.tm.appscrap.model.techmodelpage; 
import java.util.ArrayList;
import java.util.List;
public class Script{
    public int id;
    public String name;
    public String body;
    public String owner;
    public String serviceName;
    public boolean header;
    public ArrayList<Object> scriptCookies;
}

package com.tm.appscrap.model.techmodelpage; 
import java.util.ArrayList;
import java.util.List;
public class Onboarding{
    public int step;
    public boolean submitting;
    public FormData formData;
    public ArrayList<Object> errors;
}

package com.tm.appscrap.model.techmodelpage; 
import java.util.ArrayList;
import java.util.List;
public class Organization{
    public ArrayList<Object> list;
    public Object error;
    public boolean initialized;
    public int id;
    public String domain;
    public String name;
    public String logoUrl;
    public String slug;

    @Override
    public String toString() {
        return "Organization{" +
                "list=" + list +
                ", error=" + error +
                ", initialized=" + initialized +
                ", id=" + id +
                ", domain='" + domain + '\'' +
                ", name='" + name + '\'' +
                ", logoUrl='" + logoUrl + '\'' +
                ", slug='" + slug + '\'' +
                '}';
    }
}

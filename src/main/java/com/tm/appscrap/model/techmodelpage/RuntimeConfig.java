package com.tm.appscrap.model.techmodelpage; 
public class RuntimeConfig{
    public String advancedDesignBucket;
    public String advancedDesignBucketDev;
    public String customDesignsStylesVersion;
    public String privacyPolicyVersion;
    public String termsPolicyVersion;
    public String algoliaEnv;
    public String algoliaAppId;
    public String algoliaSearchApiKey;
    public String fileStackApiKey;
    public String getroAppUrl;
    public String googlePlacesToken;
    public String linkedinKey;
    public String segmentIokey;
    public String segmentAssetHost;
    public String apiBaseUrl;
    public String apiV2BaseUrl;
    public String sentryDsn;
    public String sentryEnvironment;
}

package com.tm.appscrap.model.techmodelpage; 
import java.util.ArrayList;
import java.util.List;
public class User{
    public ArrayList<Object> organizationLikeIds;
    public ArrayList<Object> organizationDiscardIds;
    public ArrayList<Object> jobLikeIds;
    public ArrayList<Object> jobDiscardIds;
    public boolean loading;
    public boolean loaded;
    public boolean showSignUpModal;
}

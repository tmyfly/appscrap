package com.tm.appscrap.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class TagsNames {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String tagsNames;

    public TagsNames(String tagsNames) {
        this.tagsNames = tagsNames;
    }

    public TagsNames() {
    }
}

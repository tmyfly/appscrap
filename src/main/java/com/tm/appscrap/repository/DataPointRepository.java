package com.tm.appscrap.repository;

import com.tm.appscrap.model.DataPoint;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DataPointRepository extends JpaRepository<DataPoint, Long> {

    Optional<DataPoint> findByPostedDateAndPositionNameAndUrlToOrganization(Long postedDate, String positionName, String urlToOrganization);
    List<DataPoint> findByCategory(String category);
    List<DataPoint> findByCategory(String category, Sort sort);
}

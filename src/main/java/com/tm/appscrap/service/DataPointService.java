package com.tm.appscrap.service;

import com.tm.appscrap.model.DataPoint;
import com.tm.appscrap.repository.DataPointRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DataPointService {
    private final DataPointRepository repository;

    public DataPointService(DataPointRepository repository) {
        this.repository = repository;
    }


    public DataPoint saveData(DataPoint dataPoint){
        return repository.save(dataPoint);
    }

    public boolean isPresent(Long postedDate, String positionName, String urlToOrganization) {
        return repository.findByPostedDateAndPositionNameAndUrlToOrganization(postedDate, positionName, urlToOrganization).isPresent();
    }

    public List<DataPoint> getAllDataPointsSorted(Sort sort) {
        return repository.findAll(sort);
    }

    public List<DataPoint> getDataPointsByCategoryAndSort(String category, Sort sort) {
        return repository.findByCategory(category, sort);
    }

    public List<DataPoint> getDataPointsByCategory(String category) {
        return repository.findByCategory(category);
    }
}
